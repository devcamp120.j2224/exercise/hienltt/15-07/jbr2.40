import com.devcamp.Person;
import com.devcamp.Staff;
import com.devcamp.Student;

public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Person 1", "TP.HCM");
        Person person2 = new Person("Person 2", "Sai Gon");
        System.out.println("Person 1: " + person1);
        System.out.println("Person 2: " + person2);

        Student student1 = new Student("Student 1", "Go Vap", "Javascript", 2021, 10000000);
        Student student2 = new Student("Student 2", "Phu Nhuan", "PHP", 2022, 12000000);
        System.out.println("Student 1: " + student1);
        System.out.println("Student 2: " + student2);

        Staff staff1 = new Staff("Staff 1", "Binh Tan, TpHCM", "IUH", 150000000);
        Staff staff2 = new Staff("Staff 1", "Tan Binh, TpHCM", "HUTECH", 16000000);
        System.out.println("staff 1 " + staff1);
        System.out.println("staff 2 " + staff2);
    }
       
}
